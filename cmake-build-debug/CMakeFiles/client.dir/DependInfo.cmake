# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/patrick/Downloads/ultimaker/src/main.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/CMakeFiles/client.dir/src/main.cpp.o"
  "/home/patrick/Downloads/ultimaker/src/test.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/CMakeFiles/client.dir/src/test.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src"
  "../deps/cpr/include"
  "../deps/nlohmann_json/single_include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
