# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/auth.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/auth.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/cookies.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/cookies.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/cprtypes.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/cprtypes.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/digest.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/digest.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/error.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/error.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/multipart.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/multipart.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/parameters.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/parameters.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/payload.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/payload.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/proxies.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/proxies.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/session.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/session.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/ssl_options.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/ssl_options.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/timeout.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/timeout.cpp.o"
  "/home/patrick/Downloads/ultimaker/deps/cpr/cpr/util.cpp" "/home/patrick/Downloads/ultimaker/cmake-build-debug/deps/cpr/cpr/CMakeFiles/cpr.dir/util.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CMAKE_USE_OPENSSL"
  "USE_SYSTEM_CURL"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../deps/cpr/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
