#pragma once

#include <cpr/cpr.h>
#include <curl/curl.h>
#include <string_view>

void using_cpr(std::string_view url) {
  auto r = cpr::Get(cpr::Url{url}, cpr::Authentication{"user", "pass"},
                    cpr::Parameters{{"anon", "true"}, {"key", "value"}});
  r.status_code;            // 200
  r.header["content-type"]; // application/json; charset=utf-8
  r.text;                   // JSON text string

    cpr::Digest dig{"user", "pass"};

}
