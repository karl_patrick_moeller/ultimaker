#include <iostream>
#include <cpr/cpr.h>
#include <curl/curl.h>
#include <cpr/curlholder.h>
#include <unistd.h>
#include <time.h>
#include <thread>
#include <chrono>
#include <nlohmann/json.hpp>

int main() {

    std::string url = "http://192.168.1.176/api/v1/";
    std::string application = "3YD MES";
    std::string user = "admin";

    std::cout << std::endl << "Connecting to:      " << url << std::endl << std::endl;
    std::cout << "Starting authentication process..." << std::endl << std::endl;
    std::cout << "Please accept: \"" << application << "\" with user: \"" << user << "\" on your Ultimaker."
              << std::endl << std::endl;

    std::string auth_id; //2a5e4718b19b4c8e06dd044e6c7ba7f9 [valid as of 21.05.19]
    std::string auth_key; //11948c8acd6d30f5165889e271aedd100f533a3b80fa3d1fbd0e0dcc6aa21a3d [valid as of 21.05.19]



    /*****************************************************
    ********** Authentication and Verification ***********
    *****************************************************/



    auto authentication = cpr::Post(cpr::Url{url + "auth/request"},
                                    cpr::Multipart{{"application", application},
                                                   {"user",        user}});

    sleep(5); //user has to verify application on the machine screen

    nlohmann::json authentication_json = nlohmann::json::parse(authentication.text); //parse response to json

    auth_id = authentication_json["id"].dump();
    auth_key = authentication_json["key"].dump();

    //std::cout << "Your ID:         "<< auth_id << std::endl;
    //std::cout << "Your Key:         "<< auth_key << std::endl;

    auto verification = cpr::Get(cpr::Url{url + "auth/check/" + auth_id},
                                 cpr::Digest{auth_id, auth_key});

    nlohmann::json verification_json = nlohmann::json::parse(verification.text);
    std::cout << "Verification result: " << verification_json["message"].dump() << std::endl << std::endl;


    // Loop to receive relevant data from the Ultimaker

    for (int i = 0; i < 5; i++) {



        /*************************************************
        ******************* System Data ******************
        *************************************************/



        auto system = cpr::Get(cpr::Url{url + "system"},
                               cpr::Digest{auth_id, auth_key});

        nlohmann::json system_json = nlohmann::json::parse(system.text);


        std::cout << "Printer:          " << system_json["variant"].dump() << std::endl;
        std::cout << "Printer GUID:     " << system_json["guid"].dump() << std::endl;



        /*************************************************
       ******************* Printer Data *****************
       *************************************************/



        auto printers = cpr::Get(cpr::Url{url + "printer"},
                                 cpr::Digest{auth_id, auth_key});

        nlohmann::json printers_json = nlohmann::json::parse(printers.text);
        std::cout << "Printer Status:   " << printers_json["status"].dump() << std::endl;



        /*************************************************
       ******************* Job Data *********************
       *************************************************/



        auto print_job = cpr::Get(cpr::Url{url + "print_job"},
                                  cpr::Digest{auth_id, auth_key});

        nlohmann::json print_job_json = nlohmann::json::parse(print_job.text);
        std::cout << "Current Job:      " << print_job_json["name"].dump() << std::endl;
        std::cout << "Job UUID:         " << print_job_json["uuid"].dump() << std::endl;
        std::cout << "Job Status:       " << print_job_json["state"].dump() << std::endl;
        std::cout << "Progress:         " << std::stod(print_job_json["progress"].dump()) * 100 << "%" << std::endl;
        std::cout << "Time left:        " <<
                  (std::stoi(print_job_json["time_total"].dump()) - std::stoi(print_job_json["time_elapsed"].dump())) /
                  60 << " minutes" << std::endl << std::endl;

        sleep(2);

    } // end of loop

    return 0;
}